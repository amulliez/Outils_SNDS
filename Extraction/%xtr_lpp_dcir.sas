/************************************************************************************************
*	Macro  : xtr_lpp_dcir																		*
*	*********************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219				*																					*
*	*********************************************************************************************
*	Description : Cette macro permet d'extraire, dans les donnees du DCIR et durant une 		*
*                 periode de temps donne, les dispositifs medicaux rembourses sur une periode	*
*				  de temps donne																*
*				  Si aucun code n'est renseigne, la macro extrait tous les dispositifs  		*
*                 realises sur la periode donnee, pour une opoulation donnee. 					*
*	*********************************************************************************************
*	Parametres :																			   	*
*		lib_out 	: nom de la bibliotheque ou sera rangee la table de sortie					*
*		tab_out 	: nom de la table de sortie													*
*		ann_deb 	: annee de debut de la periode d'extraction (format YYYY)					*
*		ann_fin 	: annee de fin de la periode d'extraction (format YYYY)						*
*		opt_pop 	: optionnel : table de population a "jointer" (ex. POP), stock� dans la 	*
*						librairie identifiee par l'option lib_prj								*
*		opt_lpp_inc : optionnel : liste des codes LPP des dispositifs a extraire				*
*		opt_lpp_exc : optionnel : liste des codes LPP des dispositifs a ne pas extraire			*
*	*********************************************************************************************
*	Remarques :																					*
*		utiliser en espace entre chaque code pour les options opt_lpp_inc / opt_lpp_exc 		*
*		pour extraire la liste de codes souhaites comme dans l'exemple ci-dessous				*
*	*********************************************************************************************
*	Resultat : table contenant les variables suivantes :								        *
*		BEN_NIR_PSA : numero d'identifiant de la personne                          				*
*		BEN_RNG_GEM : rang de naissance du beneficiaire                         				*
*		EXE_SOI_DTD : date de delivrance du dispositif		                                    *
*		LPP_COD	    : code LPP (7 caracteres) du dispositif medical								*
*		LPP_LIB	    : libelle LPP du dispositif medical									   		*
*		LPP_OGN		: origine des donnes extraites (PRS, MCO, HAD, PSY ou SSR)					*
*	*********************************************************************************************
*	Exemple :																					*
*	%xtr_lpp_dcir(lib_out = WORK, tab_out = EXTRACT_LPP, ann_deb = 2009, ann_fin = 2013, 		*
*				  opt_pop =, opt_lpp_inc = 3197753 3197750, opt_lpp_exc = );					*
*		Cet exemple permet d'extraire dans la Work dans une table appelee EXTRACT_LPP tous les	*
*		dispositifs identifies par les codes 3197750 et3197753 entre 2009 et 2013, a la fois	*
*		sur les donn�es de prestations et sur le PMSI											*
************************************************************************************************/
options nosource nonotes nosymbolgen;
option fullstimer debug=dbms_timers sastrace=',,,dq' sastraceloc=saslog nostsuffix msglevel=I;

/* Macro permettant la jointure des 9 tables de cles primaires */
%macro join(t1,t2);
		&t1..t2CT_ORt2_NUM=&t2..t2CT_ORt2_NUM ANt2 &t1..FLX_t2IS_t2Tt2=&t2..FLX_t2IS_t2Tt2 ANt2 &t1..FLX_EMT_ORt2=&t2..FLX_EMT_ORt2 ANt2
		&t1..FLX_EMT_NUM=&t2..FLX_EMT_NUM ANt2 &t1..FLX_EMT_TYP=&t2..FLX_EMT_TYP ANt2 &t1..FLX_TRT_t2Tt2=&t2..FLX_TRT_t2Tt2 ANt2
		&t1..ORt1_CLE_NUM=&t2..ORt1_CLE_NUM ANt2 &t1..PRS_ORt2_NUM=&t2..PRS_ORt2_NUM ANt2 &t1..REM_TYP_AFF=&t2..REM_TYP_AFF
%mend join;

%macro multi_like(var, lst_cod, aim);
	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);
		%let val = %qscan(&lst_cod., &i., %str( ));

		%if &aim. = INC %then
			%do;
				%if %superq(val) > 0 %then
					%do;
						%cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
					%end;

				%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
				%let ope = or;
			%end;

		%if &aim. = EXC %then
			%do;
				%if %superq(val) > 0 %then
					%do;
						%cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
					%end;

				%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
				%let ope = and;
			%end;

		%let i = %eval(&i. + 1);
	%end;
%mend;

/*Macros suppression tables ORACLE*/
%macro checkDrop(myTable);
	%if %SYSFUNC(exist(orauser.&myTable)) %then %do;
	    proc sql;
		%connectora;
		EXECUTE(
	   		drop table &myTable.
		)
		BY ORACLE;
	    quit;
	%end;
%mend;

%macro copie_POP(lib_prj);
	/*copie ref_nir_cle vers orauser*/
	%if %sysfunc(exist(orauser.&opt_pop.))=0 %then %do; 
		proc sql;
			create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
		    as select * from &lib_prj..&opt_pop.;
		quit;
	%end;
	%else %do;
		%checkDrop(&opt_pop.);
		proc sql;
			create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
		    as select * from &lib_prj..&opt_pop.;
		quit;
	%end;

%mend;

%macro xtr_lpp_dcir(lib_out, tab_out, ann_deb, ann_fin, opt_pop, opt_lpp_inc, opt_lpp_exc);
	
	/*D�finition auto ann�e archivage donn�es prestation*/
	%let ann_act = %sysfunc(year(%sysfunc(today())));
	%let aa = 2006;
	%let exist = 1;
	%do %while (&exist. = 1);
		%if %sysfunc(exist(oravue.er_prs_f_&aa.))=1 %then %do;
			%let exist = 1;
			%let aa = %eval(&aa.+1);
		%end;
		%else %do;
			%let exist = 0;
		%end;
	%end;
	%let ann_arc = %eval(&aa.-1);

	%if (&opt_lpp_inc. > 0 or &opt_lpp_exc. > 0) or (&opt_lpp_inc. = and &opt_lpp_exc. = and &opt_pop. > 0) %then %do;

		%let ann_act = %sysfunc(year(%sysfunc(today())));

		/* Donnes des dispositifs dlivrs en ambulatoire */
		%let tab_xtr_lpp = XTR_&tab_out._lpp;

		%if &opt_pop. > 0 %then %do;
			%copie_POP(lib_prj = &lib_out.);
		%end;

		%checkDrop(&tab_xtr_lpp.);

		%do aaaa = &ann_deb. %to &ann_fin.;
			%let aaaa_1 = %sysevalf(&aaaa.+1,integer);
			
			%let aa = %substr(&aaaa., 3, 2);

			%if &aaaa. <= &ann_arc. %then %do;
				%let T_PRS = ER_PRS_F_&aaaa.;
				%let T_lpp = ER_TIP_F_&aaaa.;
			%end;
			%else %do;
				%let T_PRS = ER_PRS_F;
				%let T_lpp = ER_TIP_F;
			%end;	

				%if &opt_pop. > 0 %then %do;
					%let jnt_prs_pop = '&opt_pop. t_pop inner join &T_PRS. t_prs on t_pop.BEN_NIR_PSA = t_prs.BEN_NIR_PSA';
					%let jnt_mco_pop = '&opt_pop. t_pop inner join T_MCO&aa.C T_MCOC on t_pop.BEN_NIR_PSA=T_MCOC.NIR_ANO_17';
					%let jnt_had_pop = '&opt_pop. t_pop inner join T_HAD&aa.C T_HADC on t_pop.BEN_NIR_PSA=T_HADC.NIR_ANO_17';
					%let jnt_rip_pop = '&opt_pop. t_pop inner join T_RIP&aa.C T_RIPC on t_pop.BEN_NIR_PSA=T_RIPC.NIR_ANO_17';
					%let jnt_ssr_pop = '&opt_pop. t_pop inner join T_SSR&aa.C T_SSRC on t_pop.BEN_NIR_PSA=T_SSRC.NIR_ANO_17';
				%end;
				%else %do;
					%let jnt_prs_pop = '&T_PRS. t_prs';
					%let jnt_mco_pop = ' T_MCO&aa.C T_MCOC';
					%let jnt_had_pop = ' T_HAD&aa.C T_HADC';
					%let jnt_rip_pop = ' T_RIP&aa.C T_RIPC';
					%let jnt_ssr_pop = ' T_SSR&aa.C T_SSRC';
				%end;

				%if &opt_lpp_inc. > 0 %then %do;
					%if &opt_lpp_exc. > 0 %then %do;
						%let flt_prs_lpp = '((%multi_like(var = t_lpp.TIP_PRS_IDE, lst_cod = &opt_lpp_inc., aim = INC)) and (%multi_like(var = t_lpp.TIP_PRS_IDE, lst_cod = &opt_lpp_exc., aim = EXC)))';
						%let flt_pmsi_lpp = '((%multi_like(var = t_pmsi.TIP_PRS_IDE, lst_cod = &opt_lpp_inc., aim = INC)) and (%multi_like(var = t_pmsi.TIP_PRS_IDE, lst_cod = &opt_lpp_exc., aim = EXC)))';
					%end;		
					%else %do;
						%let flt_prs_lpp = '(%multi_like(var = t_lpp.TIP_PRS_IDE, lst_cod = &opt_lpp_inc., aim = INC))';
						%let flt_pmsi_lpp = '(%multi_like(var = t_pmsi.TIP_PRS_IDE, lst_cod = &opt_lpp_inc., aim = INC))';
					%end;
				%end;
				%else %do;
					%if &opt_lpp_exc. > 0 %then %do;
						%let flt_prs_lpp = '(%multi_like(var = t_lpp.TIP_PRS_IDE, lst_cod = &opt_lpp_exc., aim = EXC))';
						%let flt_pmsi_lpp = '(%multi_like(var = t_pmsi.TIP_PRS_IDE, lst_cod = &opt_lpp_exc., aim = EXC))';
					%end;
					%else %do;
						%let flt_prs_lpp = 't_lpp.TIP_PRS_IDE is not null';
						%let flt_pmsi_lpp = 't_pmsi.TIP_PRS_IDE is not null';
					%end;
				%end;


				%if &aaaa. LE 2008 %then %do;
					%let var_dtf = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)*;
					%let var_dtd = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)-T_MCOB.SEJ_NBJ*;
				%end;
				%else %do;
					%let var_dtd = *T_MCOC.EXE_SOI_DTD*;
					%let var_dtf = *T_MCOC.EXE_SOI_DTF*;
				%end;

				/* Extraction sur les tables du PMSI, sur LPP en sus */
					%if %sysfunc(exist(oravue.T_MCO&aa.DMIP))=1 %then %do;
						proc sql;/*MCO_DMPI*/
							%connectora;
							EXECUTE(
							%if %sysfunc(exist(orauser.&tab_xtr_lpp.))=0 %then %do; create table &tab_xtr_lpp. as %end; %else %do; insert into &tab_xtr_lpp. %end;
							(select distinct T_MCOC.NIR_ANO_17 as BEN_NIR_PSA,
									%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
									T_PMSI.TIP_PRS_IDE as lpp_COD,
									'MCO' as lpp_OGN
							from %scan(&jnt_mco_pop., 1,"'")
								inner join T_MCO&aa.B T_MCOB on (T_MCOC.ETA_NUM = T_MCOB.ETA_NUM and T_MCOC.RSA_NUM = T_MCOB.RSA_NUM)
								inner join T_MCO&aa.DMIP T_PMSI on (T_MCOC.ETA_NUM = T_PMSI.ETA_NUM and T_MCOC.RSA_NUM = T_PMSI.RSA_NUM)	
							where (T_MCOC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
													    	'750100042', '750100075', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232',
													    	'750100273', '750100299', '750801441', '750803447', '750803454', '910100015', '910100023', '920100013',
													    	'920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045',
													    	'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137',
													    	'690784152', '690784178', '690787478', '830100558') /*supprimer doublons APHM APHM HCL*/
								and not(T_MCOB.GRG_RET = '024') /*supprimer RSA fictifs*/
								and T_MCOB.GRG_GHM not in ('90H01Z', '90Z00Z', '90Z01Z', '90Z02Z', '90Z03Z')/*exclusion des prestations inter �tablissements*/
								and ((T_MCOB.SEJ_TYP = 'A' or T_MCOB.SEJ_TYP is null) or (T_MCOB.SEJ_TYP = 'B' and T_MCOB.GRG_GHM not in ('28Z14Z', '28Z15Z', '28Z16Z'))) and
								%scan(&flt_pmsi_lpp., 1, "'")))
							)
							BY ORACLE;
						quit;
					%end;
					%if %sysfunc(exist(oravue.T_HAD&aa.FP))=1 %then %do;
						proc sql;/*HAD_FP*/
						%connectora;
						EXECUTE(
						%if %sysfunc(exist(orauser.&tab_xtr_lpp.))=0 %then %do; create table &tab_xtr_lpp. as %end; %else %do; insert into &tab_xtr_lpp. %end;
						(select distinct T_HADC.NIR_ANO_17 as BEN_NIR_PSA,
								T_HADC.EXE_SOI_DTD,
									T_PMSI.TIP_PRS_IDE as lpp_COD,
									'HAD' as lpp_OGN
						from %scan(&jnt_had_pop., 1,"'") 
							inner join T_HAD&aa.FP T_PMSI on (T_HADC.ETA_NUM_EPMSI = T_PMSI.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_PMSI.RHAD_NUM)
								
						where 
										  T_HADC.NIR_RET = '0' and
										  T_HADC.NAI_RET = '0' and
										  T_HADC.SEX_RET = '0' and
										  T_HADC.SEJ_RET = '0' and
										  T_HADC.FHO_RET = '0' and
										  T_HADC.PMS_RET = '0' and
										  T_HADC.DAT_RET = '0' and
										%scan(&flt_pmsi_lpp., 1, "'"))
						)
						BY ORACLE;
						quit;
					%end;


					%if %sysfunc(exist(oravue.T_RIP&aa.FP))=1 %then %do;
						proc sql;/*RIP_FP*/
							%connectora;
							EXECUTE(
							%if %sysfunc(exist(orauser.&tab_xtr_lpp.))=0 %then %do; create table &tab_xtr_lpp. as %end; %else %do; insert into &tab_xtr_lpp. %end;
							(select distinct T_RIPC.NIR_ANO_17 as BEN_NIR_PSA,
									T_RIPC.EXE_SOI_DTD,
									T_PMSI.TIP_PRS_IDE as lpp_COD,
									'PSY' as lpp_OGN
							from %scan(&jnt_rip_pop., 1,"'")
								inner join T_RIP&aa.FP T_PMSI on (T_RIPC.ETA_NUM_EPMSI = T_PMSI.ETA_NUM_EPMSI and T_RIPC.RIP_NUM = T_PMSI.RIP_NUM)	
							where %scan(&flt_pmsi_lpp., 1, "'")))		
							BY ORACLE;
						quit;
					%end;

					%if %sysfunc(exist(oravue.T_SSR&aa.FP))=1 %then %do;
						proc sql;/*SSR_FP*/
							%connectora;
							EXECUTE(
							%if %sysfunc(exist(orauser.&tab_xtr_lpp.))=0 %then %do; create table &tab_xtr_lpp. as %end; %else %do; insert into &tab_xtr_lpp. %end;
							(select distinct T_SSRC.NIR_ANO_17 as BEN_NIR_PSA,
									T_SSRC.EXE_SOI_DTD,
									T_PMSI.TIP_PRS_IDE as lpp_COD,
									'SSR' as lpp_OGN
							from %scan(&jnt_ssr_pop., 1,"'")
								inner join T_SSR&aa.B T_SSRB on (T_SSRC.ETA_NUM = T_SSRB.ETA_NUM and T_SSRC.RHA_NUM = T_SSRB.RHA_NUM)
								inner join T_SSR&aa.FP T_PMSI on (T_SSRC.ETA_NUM = T_PMSI.ETA_NUM and T_SSRC.RHA_NUM = T_PMSI.RHA_NUM)
							where (T_SSRC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
													    	'750100042', '750100075', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232',
													    	'750100273', '750100299', '750801441', '750803447', '750803454', '910100015', '910100023', '920100013',
													    	'920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045',
													    	'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137',
													    	'690784152', '690784178', '690787478', '830100558') /*supprimer doublons APHM APHM HCL*/
								and not(T_SSRB.GRG_RET = '024') /*supprimer RSA fictifs*/ and %scan(&flt_pmsi_lpp., 1, "'")))	
							)	
							BY ORACLE;
						quit;
					%end;	


			/* Via la table Prestations */
				%let date_dbt = '01/01/&aaaa.';
				%let date_dbt_prs = %sysfunc(compress(&date_dbt., '/'));
				%let date_fin = '31/12/&aaaa.';
				%let date_fin_prs = %sysfunc(compress(&date_fin., '/'));

				/* annee de flux N et flux 27 mois apres le dernier mois de prestation de l'annee*/			
				
				%do i = 0 %to 38;

					%let flxDbt=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),&i),ddmmyyn8.);
					%let flxFin=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),%eval(&i.+1)),ddmmyyn8.);

					proc sql;
					%connectora;
					EXECUTE(%if %sysfunc(exist(orauser.&tab_xtr_lpp.))=0 %then %do; create table &tab_xtr_lpp. as %end; %else %do; insert into &tab_xtr_lpp. %end; 
						(select distinct t_prs.BEN_NIR_PSA,
									    t_prs.EXE_SOI_DTD as EXE_SOI_DTD,
										t_lpp.TIP_PRS_IDE as lpp_COD,
										'PRS' as lpp_OGN
						from %scan(&jnt_prs_pop., 1,"'") inner join &T_lpp. t_lpp on (%join(t_prs,t_lpp))
						where
							%scan(&flt_prs_lpp., 1, "'") and
							t_prs.EXE_SOI_DTD between to_date(%str(%'&date_dbt_prs.%'), 'ddmmyyyy') and to_date(%str(%'&date_fin_prs.%'), 'ddmmyyyy') and
							t_prs.FLX_DIS_DTD between to_date(%str(%'&flxDbt.%'), 'ddmmyyyy') and to_date(%str(%'&flxFin.%'), 'ddmmyyyy') - 1 and
							t_prs.DPN_QLF <> 71)
					)
					BY ORACLE;	
					quit;

				%end;
		%end;

		proc sql; 
			create table &lib_out..&tab_out. as
			select BEN_NIR_PSA,
					datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10.,
					lpp_COD,
					lpp_OGN
			from orauser.&tab_xtr_lpp.;
		quit;

		%checkDrop(&tab_xtr_lpp.);

	%end;
	
	%if &opt_lpp_inc. =  and &opt_lpp_exc. =  and &opt_pop. =  %then %do;
		%put ERROR: Completer au moins une macro variable opt_lpp_inc, opt_lpp_exc ou opt_pop, pour executer la macro;
	%end;

%mend;
