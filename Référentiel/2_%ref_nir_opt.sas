
/****************************************************************************************************************
*	Macro  : ref_nir_opt															    						*
*	Date   : 11/09/2023																							*
*	*************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								*
*	*************************************************************************************************************
*	Mises � jour : (le cas �ch�ant : pr�ciser date et objet de la MAJ)											*
*																												*
*	*************************************************************************************************************
*	Description : Cette macro permet d'identifier toutes les combinaisons de sujets, indiques par l'identifiant *
*			BEN_IDT_ANO, nouvelle reference pour identifier les sujets sur le DCIR. La macro permet ainsi de	*
*			r�cup�rer tous les NIR, selon leurs modalit�s (pr�cision ci-dessous)								*
*	*************************************************************************************************************
*	Param�tres :																								*
*		lib_prj 	: nom de la biblioth�que o� sera rang�e la table de sortie									*
*		opt_diag 	: Permet de cr�er le diagramme de flux, valeur - 1 oui										*	
*	*************************************************************************************************************
*	R�sultat : table contenant les variables suivantes :														*
*		BEN_NIR_OPT : Nouvel identifiant cr��, anonymis�														*
*		BEN_IDT_ANO : Identifiant b�n�ficiaire anonymis�														*
*		BEN_NIR_PSA : Identifiant anonyme du patient dans le SNIIRAM											*	
*		BEN_RNG_GEM : Rang de naissance du b�n�ficiaire															*
*		BEN_NIR_ANO : NIR pseudonymis� du b�n�ficiaire															*
*		ASS_NIR_ANO : Matricule anonymis� de l'ouvreur de droits												*
*		BEN_CDI_NIR : Code d'identification du NIR																*
*****************************************************************************************************************
* 	Exemple	:																									*
* 	%ref_nir_opt(lib_prj = LIBABCD, opt_diag=);																	*
*	Cet exemple identifie les combinaisons indiqu�es pour un m�me identifiant BEN_IDT_ANO, avec une �criture de	*
*	la table de sortie dans la libraire LIBABCD. L'option opt_diag n'�tant pas compl�t�e, le diagramme de flux	*
*	ne sera pas cr��.																							*
****************************************************************************************************************/


%macro ref_nir_opt(lib_prj, opt_diag);

	/* 1 - Initialisation */
	options dbidirectexec dbsliceparm=(all, 4);
	proc sql;
		drop table &lib_prj..REF_NIR_OPT_2;
		drop table &lib_prj..REF_NIR_OPT_31;
		drop table ORAUSER.REF_NIR_OPT_32_1;
		drop table &lib_prj..REF_NIR_OPT_4_1;
		drop table &lib_prj..REF_NIR_OPT_4_2;
		drop table &lib_prj..REF_NIR_OPT_5_1;
		drop table &lib_prj..REF_NIR_OPT_5_2;
		drop table &lib_prj..REF_NIR_OPT_5_3;
		drop table &lib_prj..RES;
		drop table &lib_prj..REF_NIR_OPT;
	quit;


	/* 2 - Filtre sur les BEN_NIR_PSA normaux ou provisoires */
	/***
		Comme BEN_CDI_NIR est manquant pour 98 % des pseudo-NIR dans IR_BEN_R_ARC, ces derniers sont �galement consid�r�s
	***/
	proc sql;
		create table &lib_prj..REF_NIR_OPT_2 as
		select distinct BEN_NIR_PSA,
						BEN_RNG_GEM,
						BEN_NIR_ANO,
						BEN_IDT_ANO,
						BEN_IDT_ANO as BEN_NIR_OPT,
						ASS_NIR_ANO,
						BEN_SEX_COD,
						BEN_NAI_ANN,
						BEN_CDI_NIR
		from &lib_prj..REF_IR_BEN;
	quit;

	/* 3 - Filtre sur les b�n�ficiaires sans jumeaux/jumelles (cf. m�thodo CNAM-TS) */
	proc sql;
		create table &lib_prj..REF_NIR_OPT_3_1 as
		select distinct BEN_NIR_PSA, BEN_IDT_ANO
		from &lib_prj..REF_NIR_OPT_2
		group by BEN_NIR_PSA
		having count(*) > 1;
	quit;

	proc sql;
		create table &lib_prj..REF_NIR_OPT_3_2 as
		select distinct t1.BEN_NIR_OPT,
						t1.BEN_IDT_ANO,
						t1.BEN_NIR_PSA,
						t1.BEN_RNG_GEM,
						t1.BEN_NIR_ANO,
						t1.ASS_NIR_ANO,
						t1.BEN_CDI_NIR
		from &lib_prj..REF_NIR_OPT_2 t1
		where BEN_IDT_ANO not in (select BEN_IDT_ANO from &lib_prj..REF_NIR_OPT_3_1)
		order by BEN_NIR_OPT;
	quit;

	/* 4 - Anonymisation de BEN_NIR_OPT */
	data &lib_prj..REF_NIR_OPT_4_1;
		set &lib_prj..REF_NIR_OPT_3_2;
		by BEN_NIR_OPT;
		LAG_NIR_OPT = lag(BEN_NIR_OPT);
		if _N_ = 1 then LAG_NIR_OPT = BEN_NIR_OPT;
	run;
	data &lib_prj..REF_NIR_OPT_4_2;
		set &lib_prj..REF_NIR_OPT_4_1;
		retain i 1;
		if BEN_NIR_OPT ^= LAG_NIR_OPT then i = i + 1;
		NIR_OPT_ANO = i;
		drop BEN_NIR_OPT LAG_NIR_OPT i;
	run;
	data &lib_prj..REF_NIR_OPT_4_3;
		set &lib_prj..REF_NIR_OPT_4_2(rename=(NIR_OPT_ANO = BEN_NIR_OPT));
	run;


	/* 5 - Filtre sur les BEN_NIR_OPT associ�s � < 6 BEN_NIR_PSA */
	proc sql;
		create table &lib_prj..REF_NIR_OPT_5 as
		select distinct BEN_NIR_OPT
		from &lib_prj..REF_NIR_OPT_4_3
		group by BEN_NIR_OPT
		having count(*) < 6;

		create table &lib_prj..REF_NIR_OPT as
		select distinct t1.BEN_NIR_OPT,
						t2.BEN_IDT_ANO,
						t2.BEN_NIR_PSA,
						t2.BEN_RNG_GEM,
						t2.BEN_NIR_ANO,
						t2.ASS_NIR_ANO,
						t2.BEN_CDI_NIR
		from &lib_prj..REF_NIR_OPT_5 t1 inner join &lib_prj..REF_NIR_OPT_4_3 t2 on t1.BEN_NIR_OPT = t2.BEN_NIR_OPT;
	quit;

	%if &opt_diag. = 1 %then %do;
	/* 6 - R�sum� quantitatif (flowchart) de la constitution de REF_NIR_OPT */
	proc format;
		value I
		1 = 'Pseudo-NIR sniiram dans IR_BEN_R et IR_BEN_R_ARC'
		2 = 'Pseudo-NIR sniiram normaux ou provisoires'
		7 = 'Pseudo-NIR sniiram sans jumeaux/jumelles'
		8 = 'Personnes sans jumeaux/jumelles'
		9 = 'Personnes incluses dans REF_NIR_OPT'
		;
	run;
	proc sql;
		create table &lib_prj..RES as		
		select 1 as I, count(*) as N from (select distinct BEN_NIR_PSA, BEN_RNG_GEM from &lib_prj..REF_IR_BEN)
		union select 2 as I, count(*) as N from (select distinct BEN_NIR_PSA, BEN_RNG_GEM from &lib_prj..REF_NIR_OPT_2)
		union select 7 as I, count(distinct(BEN_NIR_PSA)) as N from &lib_prj..REF_NIR_OPT_4_3
		union select 8 as I, count(distinct(BEN_NIR_OPT)) as N from &lib_prj..REF_NIR_OPT_4_3
		union select 9 as I, count(distinct(BEN_NIR_OPT)) as N from &lib_prj..REF_NIR_OPT;
	quit;
	proc report data=&lib_prj..RES nowd;
		column I N;
		define I / display right '' order=data format=I.;
		define N / display right 'Effectif';
	run;
	%end;
%mend;

%ref_nir_opt(lib_prj = , opt_diag = );
