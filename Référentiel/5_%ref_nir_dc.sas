
/****************************************************************************************************************
*	Macro  : ref_nir_dc															    							*
*	Date   : 03/10/2023																							*
*	*************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								*
*	*************************************************************************************************************
*	Mises � jour : (le cas �ch�ant : pr�ciser date et objet de la MAJ)											*
*																												*
*	*************************************************************************************************************
*	Description : Cette macro permet de cr�er la table rassemblant la date de d�c�s pour chaque patient			*																								*
*	*************************************************************************************************************
*	Param�tres :																								*
*		lib_prj 	: nom de la biblioth�que o� sera rang�e la table de sortie									*
*	*************************************************************************************************************
*	R�sultat : table contenant les variables suivantes :														*
*		BEN_NIR_OPT : Nouvel identifiant cr��, anonymis�														*
*		BEN_DCD_DTE : Date de d�c�s																				*
*		DCD_CIM_COD : Cause de d�c�s																			*
*****************************************************************************************************************
* 	Exemple	:																									*
* 	%ref_nir_dc(lib_prj = LIBABCD);																				*
*	Cet exemple permet de d�finir la date de d�c�s pour chaque patient de la population issue de votre			*
*	table REF_NIR_CLE. La table de sortie est enregistr�e dans la libraire LIBABCD.								*
*																												*
*	La proc�dure proc freq permet de connaitre les effectifs exlus :											*
*		ANO_DC : nombre total de sujets exclus pour incoh�rence sur la date de d�c�s							*
*		ANO_1 : nombre de sujets exclus pour ann�e de d�c�s incoh�rente par rapport � l'ann�e de naissance		*
*				(1- si ann�e de d�c�s < ann�e de naissance)														*
*		ANO_2 : nombre de sujets exclus pour mois de d�c�s incoh�rent par rapport au mois de naissance			*
*				(1- si [ann�e de d�c�s = ann�e de naissance] et [mois de d�c�s < mois de naissance])			*
*		ANO_3 : nombre de sujets exclus pour ann�e de d�c�s incoh�rente par rapport � l'ann�e du jour			*
*				(1- si ann�e de d�c�s < ann�e du jour)															*
*		ANO_4 : nombre de sujets exclus pour mois de d�c�s incoh�rent par rapport au mois du jour				*
*				(1- si [ann�e de d�c�s = ann�e du jour] et [mois de d�c�s < mois du jour])						*
****************************************************************************************************************/


%macro ref_nir_dc(lib_prj);

/* 0 - Initialisation */
proc sql;
	drop table &lib_prj..REF_NIR_DC_1_11;
	drop table &lib_prj..REF_NIR_DC_1_12;
	drop table &lib_prj..REF_NIR_DC_1_3;
	drop table &lib_prj..REF_NIR_DC_1_4;
	drop table &lib_prj..REF_NIR_DC_2_0;
	drop table &lib_prj..REF_NIR_DC_2_1;
	drop table &lib_prj..REF_NIR_DC;
quit;
	

/* 1 - S�lection des informations sur le d�c�s des sources CNAM (IR_BEN_R) et CepiDC (KI_CCI_R) */

* 1.1 - Source CNAM (IR_BEN_R) + Si plusieurs dates de d�c�s : s�lection de la date la plus � ancienne  � ;
proc sql;
	create table REF_NIR_DC_1_11 as
	select distinct BEN_IDT_ANO,
					datepart(BEN_DCD_DTE) as BEN_DCD_DTE format ddmmyy10.
	from &lib_prj..REF_IR_BEN 
	where BEN_DCD_DTE <> '01JAN1600:00:00:00'dt and BEN_IDT_ANO is not null;
	quit;
proc sql;
	create table &lib_prj..REF_NIR_DC_1_11 as
	select distinct BEN_IDT_ANO,
					min(BEN_DCD_DTE) as BEN_DCD_DTE format ddmmyy10.
	from REF_NIR_DC_1_11
	group by BEN_IDT_ANO;
	quit;

* 1.2 - Source CepiDC (KI_CCI_R) +Si plusieurs dates de d�c�s : s�lection de la date la plus � ancienne  � ;
proc sql;
	create table REF_NIR_DC_1_12 as
	select distinct BEN_IDT_ANO,
					datepart(BEN_DCD_DTE) as BEN_DCD_DTE format ddmmyy10.,
					DCD_CIM_COD
	from ORAVUE.KI_CCI_R 
	where BEN_DCD_DTE is not null and BEN_IDT_ANO is not null;
	quit;
proc sql;
	create table &lib_prj..REF_NIR_DC_1_12 as
	select distinct BEN_IDT_ANO,
					min(BEN_DCD_DTE) as BEN_DCD_DTE format ddmmyy10.,
					DCD_CIM_COD
	from REF_NIR_DC_1_12
	group by BEN_IDT_ANO;
	quit;

/* 1.3 - Priorisation de la source CepiDC si la date de d�c�s y est renseign�e, 
	sinon date de d�c�s du r�f�rentiel REF_IR_BEN retenue*/
proc sql;
	create table &lib_prj..REF_NIR_DC_1_3 as
	select distinct t1.BEN_NIR_OPT,
					case when t3.BEN_DCD_DTE is not null then t3.BEN_DCD_DTE
					else t2.BEN_DCD_DTE
					end as BEN_DCD_DTE format ddmmyy10.,
					t3.DCD_CIM_COD
	from &lib_prj..REF_NIR_CLE t1 left join &lib_prj..REF_NIR_DC_1_11 t2 on t1.BEN_IDT_ANO = t2.BEN_IDT_ANO
									left join &lib_prj..REF_NIR_DC_1_12 t3 on t1.BEN_IDT_ANO = t3.BEN_IDT_ANO;
	quit;
proc sql;
	create table &lib_prj..REF_NIR_DC_1_4 as
	select distinct *
	from &lib_prj..REF_NIR_DC_1_3
	where BEN_DCD_DTE is not null;
	quit;
						

/* 2 - Gestion des valeurs aberrantes */

proc sql;
	create table &lib_prj..REF_NIR_DC_2_0 as
	select distinct *
	from &lib_prj..REF_NIR_DEM t1 inner join &lib_prj..REF_NIR_DC_1_4 t2 on t1.BEN_NIR_OPT = t2.BEN_NIR_OPT
	where BEN_DCD_DTE<>.;
	quit;
	* avec une date de d�c�s et OK d�mo (sexe, naissance);

data &lib_prj..REF_NIR_DC_2_1;
	set &lib_prj..REF_NIR_DC_2_0;
	if year(BEN_DCD_DTE) < BEN_NAI_ANN then ANO_1=1;
	if year(BEN_DCD_DTE) = BEN_NAI_ANN and month(BEN_DCD_DTE) < BEN_NAI_MOI then ANO_2=1;
	if year(BEN_DCD_DTE) > %sysfunc(year(%sysfunc(today()))) then ANO_3=1;
	if year(BEN_DCD_DTE) = %sysfunc(year(%sysfunc(today())))and month(BEN_DCD_DTE) > %sysfunc(month(%sysfunc(today()))) then ANO_4=1;
	if ANO_1=1 or ANO_2=1 or ANO_3=1 or ANO_4=1 then ANO_DC=1;
	run;
proc freq data=&lib_prj..REF_NIR_DC_2_1;
	tables ANO_DC ANO_1 ANO_2 ANO_3 ANO_4 / missing;
	run;	
	
proc sql;
	create table &lib_prj..REF_NIR_DC as
	select distinct BEN_NIR_OPT, BEN_DCD_DTE, DCD_CIM_COD
	from &lib_prj..REF_NIR_DC_2_1
	where ANO_DC=.;
	quit;

/* 3 - Nettoyage */

proc sql;
	drop table &lib_prj..REF_NIR_DC_1_11;
	drop table &lib_prj..REF_NIR_DC_1_12;
	drop table &lib_prj..REF_NIR_DC_1_3;
	drop table &lib_prj..REF_NIR_DC_1_4;
	drop table &lib_prj..REF_NIR_DC_2_0;
	drop table &lib_prj..REF_NIR_DC_2_1;
quit;

%mend;

%ref_nir_dc(lib_prj = );
